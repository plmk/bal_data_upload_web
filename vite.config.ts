import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import VueSetupExtend from 'vite-plugin-vue-setup-extend';
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers';
import path from "path"
export default defineConfig({

	base: './',
	build: {
		sourcemap: false,
	},
	server: {
		host: '0.0.0.0',
		port: 8888,
		proxy: {
			'/request': {
				target: 'http://127.0.0.1:8204/',
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/request/, ''),
			},
		},
	},
	define: {
		'process.env': {
			'BASE_API': "/request"
		}
	},
	plugins: [
		vue(),
		VueSetupExtend(),
		AutoImport({
			resolvers: [ElementPlusResolver()],
			imports: ["vue", "vue-router", "pinia",], // 自动导入vue和vue-router相关函数
			dts: "src/auto-import.d.ts" // 生成 `auto-import.d.ts` 全局声明
		}),
		Components({
			resolvers: [ElementPlusResolver(),
				 AntDesignVueResolver({
				importStyle: false, // css in js
			}),]
		}),
	],

	resolve: {
        // Vite路径别名配置
        alias: {
            '@': path.resolve('./src')
        }
    },

	optimizeDeps: {
		include: ['schart.js']
	},
	
});
