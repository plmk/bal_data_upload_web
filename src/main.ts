import { createApp } from 'vue';
import { createPinia } from 'pinia';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import App from './App.vue';
import router from './router';
import 'element-plus/dist/index.css';
import './assets/css/icon.css';
import infiniteScroll from 'vue3-infinite-scroll-better'
const app = createApp(App);
app.config.globalProperties.$d = 'd'
import $ from 'jquery'
createApp(App).use($)
app.use(createPinia());
app.use(router)
app.use(infiniteScroll);

// 注册elementplus图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
}
// 自定义权限指令


app.mount('#app');
