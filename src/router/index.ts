import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router';
import Home from '../views/home.vue';

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        redirect: '/gantan',
    },
    {
        path: '/',
        name: 'Home',
        component: Home,
        children: [           
            {
                path: '/gantan',
                name: 'gantan',
                meta: {
                    title: '干滩设备',
                },
                component: () => import( '../views/gantan/index.vue'),
            },
            {
                path: '/yuliang',
                name: 'yuliang',
                meta: {
                    title: '降雨量设备',
                },
                component: () => import('../views/yuliang/index.vue'),
            },
            {
                path: '/wuwei',
                name: 'wuwei',
                meta: {
                    title: '库水位设备',
                },
                component: () => import( '../views/wuwei/index.vue'),
            },
            {
                path: '/shenya',
                name: 'shenya',
                meta: {
                    title: '浸润线设备',
                },
                component: () => import( '../views/shenya/index.vue'),
            },
            {
                path: '/gnss',
                name: 'gnss',
                meta: {
                    title: 'GPS设备',
                },
                component: () => import('../views/gnss/index.vue'),
            },
            {
                path: '/cexie',
                name: 'cexie',
                meta: {
                    title: '内部测斜设备',
                },
                component: () => import( '../views/cexie/index.vue'),
            },
        ],
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} | 矿山设备平台`;
    next();
});

export default router;
