import axios from 'axios';
import qs from 'qs';
import 'es6-promise';
import { ElMessage, ElMessageBox } from 'element-plus';
import until from './until.js';
axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8';
axios.defaults.baseURL = '/';
axios.defaults.baseURL = import.meta.env.VITE_API_BASE_URL;

let error_codeList = until.getErrCode();
let app_power_sign = '';

export function setHeader(methods, key, value) {
	app_power_sign = value;
}

export function setHeaderClear(methods) {
	app_power_sign = '';
}

export function replaceUrl(url, type) {
	return url;
}

// 删除请求
export function delFun(url, params, slash) {
	return new Promise((resolve, reject) => {
		axios.delete(url, {
			params: params,
			headers: {
				'Authorization': "",
				'signature': "",
			}
		}).then(res => {
			if (res.data.error_code === 0) {
				if (res.data.data === null) {
					res.data.data = [];
				}
				resolve(res.data);
			}
		}).catch(err => {
			
				handleErrorResponse(err, resolve);
		});
	});
}
// GET请求
export function get(url, params) {
	return new Promise((resolve, reject) => {
		axios.get(url, {
			params: params,
			headers: {
				'Authorization': "",
				'signature': "",
			}
		}).then(res => {
			if (res.data.error_code === 0) {
				if (res.data.data === null) {
					res.data.data = [];
				}
				resolve(res.data);
			}
		}).catch(err => {	
				handleErrorResponse(err, resolve);
		});
	});
}

// PUT请求
export function put(url, data) {
	return new Promise((resolve, reject) => {
		axios.put(url, qs.stringify(data), {
			headers: {
				'Authorization': "",
				'Content-Type': 'application/x-www-form-urlencoded',
				'signature': "",
				'AppPower': app_power_sign,
			}
		}).then(res => {
			if (res.data.error_code === 0) {
				if (res.data.data === null) {
					res.data.data = [];
				}
				resolve(res.data);
			}
		}).catch(err => {
			if (err.response.status === 401) {				
				handleErrorResponse(err, resolve);
			}
		});
	});
}

// POST请求
export function post(url, data, bodyData) {
	let ContentType = "application/x-www-form-urlencoded";
	let postData = qs.stringify(data);
	if (bodyData !== undefined) {
		ContentType = "application/json;charset=utf-8";
		postData = bodyData;
	}

	return new Promise((resolve, reject) => {
		axios.post(url, postData, {
			headers: {
				'Content-Type': ContentType,
				'Authorization': "",
				'signature': "",
			}
		}).then(res => {
			if (res.data.error_code === 0) {
				if (res.data.data === null) {
					res.data.data = [];
				}
				resolve(res.data);
			}
		}).catch(err => {
				handleErrorResponse(err, resolve);
		});
	});
}

export function upload(url, data, bodyData) {
	// 这里可以处理文件上传的请求
}

function handleErrorResponse(err, resolve) {
	let errorMessage = "程序出错了";

	try {
		if (err.response.data.msg) {
			errorMessage = err.response.data.msg;
		}

		ElMessage({
			message: errorMessage,
			type: 'error'
		});
	} catch {
		ElMessage({
			message: "程序出错了",
			type: 'error'
		});
	}

	console.error("Error Details:", err.response.data);

	resolve({ error_code: err.response.data.error_code || 1, message: errorMessage });
}