declare const until: {
    setCookie(name: string, value: string): void;
    getCookie(name: string): string | null;
    delCookie(name: string): void;
    getParameter(variable: string): string | false;
    getNowDayTime(): number;
    getAddDayTime(num: number): number;
    guid(): string;
    generateRandomString(num:number):string;
    getAddDayFirst(num: number): number;
    getAddDayLast(num: number): number;
    getDayFirst(date: string | number | Date): Date;
    getDayLast(date: string | number | Date): Date;
    getNowDay(): string;
    getAddDay(num: number): string;
    getNowWeekBegin(): string;
    getNewAddDay(oldDate: string, num: number): string;
    getYMDNow(): string;
    formatDate(date: string | number | Date): string;
    formatDayDate(date: string | number | Date): string;
    formatDateDay(date: string | number | Date): string;
    formatDateHour(date: string | number | Date): string;
    formatNoyearDateHour(date: string | number | Date): string;
    dateHour(date: string | number | Date): string;
    intervalTime(startTime: number, endTime: number): string;
    getWid(): string;
    isJsonString(str: string): boolean;
    getTimeNow(time: number): string;
    getEditorOption(): any;
    getErrCode(): string[];
    isdoDecrypt(data:string):any;
    getRandomSixDigit():any;
    getDateString(date: string | number | Date): string;
  };
  
  export default until;
  