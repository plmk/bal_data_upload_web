import {get,post,upload,delFun,put,replaceUrl,setHeader,setHeaderClear} from './getpost'  
// ==========================================================================
// 生成日期：2024-02-06 15:23:34
// ==========================================================================

// 不同的请求域名依靠replaceUrl来做，第二个参数来做平台标识

function thisReplaceUrl(url){
    return replaceUrl(url,'')
}


let BAL_DATA_UPLOADCEXIE_DATAGET = "/bal_data_upload/cexie_data" // => 获取分页列表
let BAL_DATA_UPLOADCEXIE_LISTGET = "/bal_data_upload/cexie_list" // => 获取分页列表
let BAL_DATA_UPLOADDEVICE_LINK_UPPUT = "/bal_data_upload/device_link_up" // => 更新设备关联
let BAL_DATA_UPLOADGANTAN_DATAGET = "/bal_data_upload/gantan_data" // => 获取分页列表
let BAL_DATA_UPLOADGANTAN_LISTGET = "/bal_data_upload/gantan_list" // => 获取分页列表
let BAL_DATA_UPLOADGNSS_DATAGET = "/bal_data_upload/gnss_data" // => 获取分页列表
let BAL_DATA_UPLOADGNSS_LISTGET = "/bal_data_upload/gnss_list" // => 获取分页列表
let BAL_DATA_UPLOADPROJECT_INFOGET = "/bal_data_upload/project_info" // => 获取列表
let BAL_DATA_UPLOADSHENYA_DATAGET = "/bal_data_upload/shenya_data" // => 获取分页列表
let BAL_DATA_UPLOADSHENYA_LISTGET = "/bal_data_upload/shenya_list" // => 获取分页列表
let BAL_DATA_UPLOADWUWEI_DATAGET = "/bal_data_upload/wuwei_data" // => 获取分页列表
let BAL_DATA_UPLOADWUWEI_LISTGET = "/bal_data_upload/wuwei_list" // => 获取分页列表
let BAL_DATA_UPLOADYULIANG_DATAGET = "/bal_data_upload/yuliang_data" // => 获取分页列表
let BAL_DATA_UPLOADYULIANG_LISTGET = "/bal_data_upload/yuliang_list" // => 获取分页列表

let BAL_DATA_UPLOADMENUGET = "/bal_data_upload/menu" //菜单 => 获取PC端的菜单


const  bal_data_upload = {

    /**
    * =>获取列表
    *
    **/
    Bal_data_uploadProject_infoGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADPROJECT_INFOGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadProject_infoGet');
        //相关参数
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    *
    **/
    Bal_data_uploadWuwei_listGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADWUWEI_LISTGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadWuwei_listGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    * @param string project_code query false 项目编号 
    *
    **/
    Bal_data_uploadGnss_listGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADGNSS_LISTGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadGnss_listGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        getParam.project_code = typeof(data.project_code) !="undefined" ?  data.project_code:''  
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    *
    **/
    Bal_data_uploadYuliang_listGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADYULIANG_LISTGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadYuliang_listGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    *
    **/
    Bal_data_uploadGantan_listGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADGANTAN_LISTGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadGantan_listGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    *
    **/
    Bal_data_uploadShenya_listGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADSHENYA_LISTGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadShenya_listGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    * @param string project_code query false 项目编号 
    *
    **/
    Bal_data_uploadCexie_listGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADCEXIE_LISTGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadCexie_listGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        getParam.project_code = typeof(data.project_code) !="undefined" ?  data.project_code:''  
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    * @param string device_code query true 设备标识 
    *
    **/
    Bal_data_uploadYuliang_dataGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADYULIANG_DATAGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadYuliang_dataGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        getParam.device_code = data.device_code    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    * @param string device_code query true 设备标识 
    *
    **/
    Bal_data_uploadGnss_dataGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADGNSS_DATAGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadGnss_dataGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        getParam.device_code = data.device_code    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    * @param string device_code query true 设备标识 
    *
    **/
    Bal_data_uploadGantan_dataGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADGANTAN_DATAGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadGantan_dataGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        getParam.device_code = data.device_code    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    * @param string device_code query true 设备标识 
    *
    **/
    Bal_data_uploadShenya_dataGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADSHENYA_DATAGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadShenya_dataGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        getParam.device_code = data.device_code    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    * @param string device_code query true 设备标识 
    *
    **/
    Bal_data_uploadCexie_dataGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADCEXIE_DATAGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadCexie_dataGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        getParam.device_code = data.device_code    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>获取分页列表
    * @param integer page query true 分页数 
    * @param integer each_page query true 分页读取数量 
    * @param string device_code query true 设备标识 
    *
    **/
    Bal_data_uploadWuwei_dataGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADWUWEI_DATAGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadWuwei_dataGet');
        //相关参数
        getParam.page = data.page    
        getParam.each_page = data.each_page    
        getParam.device_code = data.device_code    
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

    /**
    * =>更新设备关联
    * @param string device_code formData false 本地设备标识 
    * @param string platform_device_name formData false 平台设备名称 
    * @param string platform_device_code formData false 平台设备标识 
    *
    **/
    Bal_data_uploadDevice_link_upPut(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADDEVICE_LINK_UPPUT)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadDevice_link_upPut');
        //相关参数
        postParam.device_code  = typeof(data.device_code) !="undefined" ?  data.device_code:''  
        postParam.platform_device_name  = typeof(data.platform_device_name) !="undefined" ?  data.platform_device_name:''  
        postParam.platform_device_code  = typeof(data.platform_device_code) !="undefined" ?  data.platform_device_code:''  
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = put(url,postParam,bodyData) 
        return result;
    },

    /**
    * 菜单=>获取PC端的菜单
    *
    **/
    Bal_data_uploadMenuGet(data,bodyData){
        let url = thisReplaceUrl(BAL_DATA_UPLOADMENUGET)
        let getParam  = new Object;
        let postParam = new Object;
        setHeaderClear('Bal_data_uploadMenuGet');
        //相关参数
        
        
        //请求服务
        let arr = Object.keys(getParam);
        if(arr.length != 0){
            let g = 0
            for(let key in getParam){
                if(g == 0){
                    url = url +"?"+key+"="+getParam[key]
                }else{
                    url = url +"&"+key+"="+getParam[key]
                }
                g++
            }
        }
        const result = get(url,null,bodyData) 
        return result;
    },

}
export default  bal_data_upload;